sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/odata/v2/ODataModel"
], function (Controller,ODataModel) {
	"use strict";

	return Controller.extend("Tutoriales.ODataV2.controller.View1", {
		onInit: function () {
		var oModel = new ODataModel("/destinations/northwind/V2/Northwind/Northwind.svc/");
		var oMetadata = oModel.getServiceMetadata();
		}
	});
});